package mk.iwec.service;

import java.util.Map;

import org.springframework.stereotype.Service;

import mk.iwec.model.Student;

@Service
public interface StudentService {

	public Map<Integer, Student> getStudents();

	public Student getStudentById(Integer id);

	public void addNewStudent(Integer id, String firstName, String lastName);

	public void deleteStudent(Integer id);

	public void updateStudent(Integer id, String firstName, String lastName);
}

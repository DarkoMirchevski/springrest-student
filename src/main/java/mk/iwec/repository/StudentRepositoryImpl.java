package mk.iwec.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import mk.iwec.model.Student;


@Repository
public class StudentRepositoryImpl implements StudentRepository {
	private static final String SQL_SELECT_ALL = "select * from students";
	private static final String SQL_SELECT_BYID = "SELECT first_name, last_name from students where id = ?";
	private static final String SQL_INSERT = "INSERT INTO students (id, first_name, last_name) VALUES (?, ?, ?)";
	private static final String SQL_DELETE_BYID = "DELETE FROM students WHERE id = ?";
	private static final String SQL_UPDATE = "UPDATE students set first_name = ?, last_name = ? WHERE id = ?";

	@Override
	public Map<Integer, Student> getStudents() {
		Map<Integer, Student> studentMap = new HashMap<>();
		try (DB db = new DB(); Statement st = db.getConn().createStatement();) {
			ResultSet rs = st.executeQuery(SQL_SELECT_ALL);
			while (rs.next()) {
				studentMap.put(rs.getInt("id"), new Student(rs.getString("first_name"), rs.getString("last_name")));

			}
		} catch (SQLException e) {
			e.printStackTrace();

		} catch (Exception e1) {

			e1.printStackTrace();
		}
		return studentMap;
	}

	@Override
	public Student getStudentById(Integer id) {
		if (id == null) {
			throw new IllegalStateException("id cant be null");
		}

		Student result = null;
		try (DB db = new DB(); PreparedStatement ps = db.getConn().prepareStatement(SQL_SELECT_BYID);) {
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result = new Student(rs.getString("first_name"), rs.getString("last_name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;

	}

	public void addNewStudent(Integer id, String firstName, String lastName) {
		if (id == null) {
			throw new IllegalStateException("id cant be null");
		}

		try (DB db = new DB(); PreparedStatement ps = db.getConn().prepareStatement(SQL_INSERT);) {
			ps.setInt(1, id);
			ps.setString(2, firstName);
			ps.setString(3, lastName);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteStudent(Integer id) {
		if (id == null) {
			throw new IllegalStateException("id cant be null");
		}

		try (DB db = new DB(); PreparedStatement ps = db.getConn().prepareStatement(SQL_DELETE_BYID);) {
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void updateStudent(Integer id, String firstName, String lastName) {
		if (id == null) {
			throw new IllegalStateException("id cant be null");
		}

		try (DB db = new DB(); PreparedStatement ps = db.getConn().prepareStatement(SQL_UPDATE);) {
			ps.setString(1, firstName);
			ps.setString(2, lastName);
			ps.setInt(3, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}

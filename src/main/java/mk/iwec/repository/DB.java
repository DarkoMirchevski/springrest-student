package mk.iwec.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DB implements AutoCloseable {
	private Connection conn;

	public DB() {
		String ip = "127.0.0.1";
		int port = 5432;
		String dbName = "StudentRESTfull";
		String username = "postgres";
		String password = "075648917";
		String url = "jdbc:postgresql://" + ip + ':' + port + '/' + dbName;

		try {
			conn = DriverManager.getConnection(url, username, password);
		} catch (IllegalArgumentException | SecurityException | SQLException e) {
			e.printStackTrace();
		}

	}

	public Connection getConn() {
		return conn;
	}

	@Override
	public void close() throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}

}

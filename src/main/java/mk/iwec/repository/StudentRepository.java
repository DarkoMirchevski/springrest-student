package mk.iwec.repository;

import java.util.Map;

import mk.iwec.model.Student;

public interface StudentRepository {

	public Map<Integer, Student> getStudents();

	public Student getStudentById(Integer id);

	public void addNewStudent(Integer id, String firstName, String lastName);

	public void deleteStudent(Integer id);

	public void updateStudent(Integer id, String firstName, String lastName);

}

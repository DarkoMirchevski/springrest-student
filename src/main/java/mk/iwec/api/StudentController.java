package mk.iwec.api;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import mk.iwec.model.Student;
import mk.iwec.service.StudentService;

@AllArgsConstructor
@RestController
@RequestMapping("student")
public class StudentController {

	@Autowired
	private final StudentService studentService;

	@GetMapping //HTTP-req example http://localhost:8080/student
	public Map<Integer, Student> getStudents() {
		return studentService.getStudents();

	}

	@GetMapping(path = "/{id}") //HTTP-req example http://localhost:8080/student/111
	public Student getStudentById(@PathVariable("id") Integer id) {

		return studentService.getStudentById(id);

	}

	@PostMapping //HTTP-req example http://localhost:8080/student?id=111&firstName=Darko&lastName=Mirchevski
	@ResponseStatus(value = HttpStatus.CREATED)
	public void registerNewStudent(@RequestParam(required = true) Integer id,
			@RequestParam(required = true) String firstName, @RequestParam(required = true) String lastName) {
		studentService.addNewStudent(id, firstName, lastName);
	}

	@DeleteMapping(path = "/{id}") // http://localhost:8080/student/2
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteStudent(@PathVariable("id") Integer id) {
		studentService.deleteStudent(id);
	}

	@PutMapping(path = "/{id}") //HTTP-req example http://localhost:8080/student/111?firstName=Darko&lastName=Testero
	@ResponseStatus(value = HttpStatus.OK)
	public void updateStudent(@PathVariable("id") Integer id, @RequestParam(required = true) String firstName,
			@RequestParam(required = true) String lastName) {
		studentService.updateStudent(id, firstName, lastName);

	}

}

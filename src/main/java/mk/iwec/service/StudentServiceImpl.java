package mk.iwec.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import mk.iwec.exception.StudentNotFoundException;
import mk.iwec.model.Student;
import mk.iwec.repository.StudentRepository;

@AllArgsConstructor
@Service
public class StudentServiceImpl implements StudentService {
	@Autowired
	private final StudentRepository studentRepository;

	public Map<Integer, Student> getStudents() {
		return this.studentRepository.getStudents();
	}

	public Student getStudentById(Integer id) {
		if (this.studentRepository.getStudentById(id) == null) {
			throw new StudentNotFoundException("student is not present");
		}

		return this.studentRepository.getStudentById(id);

	}

	public void addNewStudent(Integer id, String firstName, String lastName) {
		if (this.studentRepository.getStudentById(id) == null) {
			this.studentRepository.addNewStudent(id, firstName, lastName);
		} else {
			throw new StudentNotFoundException("student is present");
		}

	}

	public void deleteStudent(Integer id) {
		if (this.studentRepository.getStudentById(id) == null) {
			throw new StudentNotFoundException("student is not present");
		}
		this.studentRepository.deleteStudent(id);
	}

	public void updateStudent(Integer id, String firstName, String lastName) {
		if (this.studentRepository.getStudentById(id) == null) {
			throw new StudentNotFoundException("student is not present");
		}
		this.studentRepository.updateStudent(id, firstName, lastName);
	}

}
